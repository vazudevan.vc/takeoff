<!DOCTYPE html>
<html lang="zxx">
    
<!-- Mirrored from keenitsolutions.com/products/html/edulearn/edulearn-demo/index5.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Jan 2020 15:24:31 GMT -->
<head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Edulearn | Responsive Education HTML5 Template</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="<?php echo base_url()?>assets/apple-touch-icon.html">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>assets/images/fav5.png">
        <!-- bootstrap v4 css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/animate.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/owl.carousel.css">
		<!-- slick css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/slick.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/magnific-popup.css">
		<!-- Offcanvas CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/off-canvas.css">
		<!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/fonts/flaticon.css">
		<!-- flaticon2 css  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/fonts/fonts2/flaticon.css">
        <!-- rsmenu CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/rsmenu-main.css">
        <!-- rsmenu transitions CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/rsmenu-transitions.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/style.css">
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/responsive.css">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="home5">
        <!--Preloader area start here-->
        <div class="book_preload">
            <div class="book">
                <div class="book__page"></div>
                <div class="book__page"></div>
                <div class="book__page"></div>
            </div>
        </div>
		<!--Preloader area end here-->
		
        <!--Full width header Start-->
		<div class="full-width-header">
			<!-- Toolbar Start -->
			<div class="rs-toolbar">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="rs-toolbar-left">
								<div class="welcome-message">
									<i class="glyph-icon flaticon-placeholder"></i>
                                    <span>Edulearn NYC, USA</span> 
								</div>
								<div class="welcome-message">
									<i class="glyph-icon flaticon-phone-call"></i>
                                    <span><a href="tel:1234567890">+1234-567-890</a></span> 
								</div>
								<div class="welcome-message">
									<i class="glyph-icon flaticon-email"></i>
                                    <span><a href="mailto:info@domain.com">info@domain.com</a></span> 
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="rs-toolbar-right">
								<div class="toolbar-share-icon">
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
								<a class="hidden-xs rs-search" data-target=".search-modal" data-toggle="modal" href="#">
                                            <i class="fa fa-search"></i>
                                        </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Toolbar End -->
			
			<!--Header Start-->
			<header id="rs-header" class="rs-header">
				<!-- Menu Start -->
				<div class="menu-area menu-sticky">
					<div class="container">
				        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="logo-area">
                                    <a href="<?php echo base_url()?>assets/index.html"><img src="<?php echo base_url()?>assets/images/logo2.png" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12">
                                <div class="main-menu">
                                    <!-- <div id="logo-sticky" class="text-center">
                                        <a href="index.html"><img src="images/logo.png" alt="logo"></a>
                                    </div> -->
                                    <a class="rs-menu-toggle"><i class="fa fa-bars"></i>Menu</a>
                                    <nav class="rs-menu">
                                        <ul class="nav-menu">
                                            <!-- Home -->
                                            <li class="current-menu-item current_page_item menu-item-has-children"> <a href="#" class="home">Home</a>
                                              <ul class="sub-menu">
                                                <li><a href="<?php echo base_url()?>assets/index.html">Home One</a> </li>
                                                <li><a href="<?php echo base_url()?>assets/index2.html">Home Two</a> </li>
                                                <li><a href="<?php echo base_url()?>assets/index3.html">Home Three</a></li>
                                                <li><a href="<?php echo base_url()?>assets/index4.html">Home Four</a> </li>
                                                <li class="active"><a href="index5.html">Home Five</a> </li>
                                                <li><a href="<?php echo base_url()?>assets/instructor-home.html">Home Instructor</a> </li>
                                              </ul>
                                            </li>
                                            <!-- End Home --> 

                                            <!--About Menu Start-->
                                            <li class="menu-item-has-children"> <a href="#">About Us</a>
                                                <ul class="sub-menu">
                                                    <li> <a href="<?php echo base_url()?>assets/about.html">About One</a></li>
                                                    <li><a href="<?php echo base_url()?>assets/about2.html">About Two</a></li>
                                                    <li><a href="<?php echo base_url()?>assets/about3.html">About Three</a></li>
                                                </ul>
                                            </li>
                                            <!--About Menu End--> 

                                            <!-- Drop Down -->
                                            <li class="menu-item-has-children"> <a href="#">Pages</a>
                                                <ul class="sub-menu"> 

                                                    <li class="menu-item-has-children"> <a href="#">Teachers</a>
                                                      <ul class="sub-menu">
                                                        <li> <a href="<?php echo base_url()?>assets/teachers.html">Teachers</a> </li>
                                                        <li> <a href="<?php echo base_url()?>assets/teachers-without-filter.html">Teachers Without Filter</a> </li> 
                                                        <li> <a href="<?php echo base_url()?>assets/teachers-single.html">Teachers Single</a> </li>
                                                      </ul>
                                                    </li>

                                                    <li class="menu-item-has-children"> <a href="#">Gallery</a>
                                                      <ul class="sub-menu">
                                                        <li> <a href="<?php echo base_url()?>assets/gallery.html">Gallery One</a> </li>
                                                        <li> <a href="<?php echo base_url()?>assets/gallery2.html">Gallery Two</a> </li> 
                                                        <li> <a href="<?php echo base_url()?>assets/gallery3.html">Gallery Three</a> </li>
                                                      </ul>
                                                    </li>

                                                    <li class="menu-item-has-children"> <a href="#">Shop</a>
                                                      <ul class="sub-menu">
                                                        <li> <a href="<?php echo base_url()?>assets/shop.html">Shop</a> </li> 
                                                        <li> <a href="<?php echo base_url()?>assets/shop-details.html">Shop Details</a> </li>
                                                      </ul>
                                                    </li>

                                                    <li><a href="<?php echo base_url()?>assets/cart.html">Cart</a></li>
                                                    <li><a href="<?php echo base_url()?>assets/checkout.html">Checkout</a></li>

                                                    <li><a href="<?php echo base_url()?>assets/error-404.html">Error 404</a></li>
                                                </ul>
                                            </li>
                                            <!--End Icons -->

                                            <!--Courses Menu Start-->
                                            <li class="menu-item-has-children"> <a href="#">Courses</a>
                                              <ul class="sub-menu">
                                                <li><a href="<?php echo base_url()?>assets/courses.html">Courses One</a></li>
                                                <li><a href="<?php echo base_url()?>assets/courses2.html">Courses Two</a></li>
                                                <li><a href="<?php echo base_url()?>assets/courses-details.html">Courses Details</a></li>
                                                  <li><a href="<?php echo base_url()?>assets/courses-details2.html">Courses Details 2</a></li>
                                              </ul>
                                            </li>
                                            <!--Courses Menu End-->

                                            <!--Events Menu Start-->
                                            <li class="menu-item-has-children"> <a href="#">Events</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?php echo base_url()?>assets/events.html">Events</a></li>
                                                    <li><a href="<?php echo base_url()?>assets/events-details.html">Events Details</a></li>
                                                </ul>
                                            </li>
                                            <!--Events Menu End-->

                                            <!--blog Menu Start-->
                                            <li class="menu-item-has-children"> <a href="#">Blog</a>
                                                <ul class="sub-menu">
                                                    <li><a href="<?php echo base_url()?>assets/blog.html">Blog</a></li>
                                                    <li><a href="<?php echo base_url()?>assets/blog-details.html">Blog Details</a></li>
                                                </ul>
                                            </li>
                                            <!--blog Menu End-->

                                            <!--Contact Menu Start-->
                                            <li> <a href="<?php echo base_url()?>assets/contact.html">Contact</a></li>
                                            <!--Contact Menu End-->
                                        </ul>
                                    </nav>
                                    <div class="apply-box">
                                        <a href="#" class="apply-btn">Apply Now</a>
                                    </div>
                            </div>
                        </div>
						</div>
					</div>
				</div>
				<!-- Menu End -->
			</header>
			<!--Header End-->

		</div>
        <!--Full width header End-->
